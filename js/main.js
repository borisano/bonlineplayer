$(function() {
    
    refreshVideosList();

}); // endof document.ready

/**
 * Public visible function that recieves and displays list of videos.
 * Also assigns click listeners for list items
 * 
 * @param {string} token - token to be sent as X-Auth-Token parameter of request
 */
function refreshVideosList(token) {
    (function(){
        // we can't get list of videos without token
        if( !token ) { 
            login();
            return;
        }
        
        // recieve list of videos
        $.ajax({
            url: "http://vacancy.dev.telehouse-ua.net/media/list",
            crossDomain: true,
            context: document.body,
            type: 'GET',
            headers: {
                'X-Auth-Token' : token
            },
            success : function( response ) {
                updateVideosList( response.list );
            },
            error : function() {
                //possibly error occures becouse of inproper token. Try to relogin to resolve it
                showErrorMessage( 'При загрузке списка видеофайлов произошла ошибка');
                login();
            }
        }); // endof media/list ajax 
        
        
        /**
         * Using provided videos array, build and show videos list.
         * Assign handler for every video
         * 
         * @param {Array} videos - array of objects with such structure:
         *                      {title, artist, genre, aurl, iurl }
         */
        function updateVideosList( videos ) {
            var $videosList = $("ul#videos-list");
            $videosList.find('li:visible').remove();
            
            for( var i = 0; i < videos.length; i++ ) {
                // form another video's list item and pull into the list
                var $newLi = $videosList.find('li:first').clone();
      
                $newLi.find('span.title')   .text( videos[i].title );
                $newLi.find('span.artist')  .text( videos[i].artist );
                $newLi.find('span.genre')   .text( videos[i].genre );
                $newLi.find('span.aurl')    .text( videos[i].aurl );

                $newLi.show().appendTo( $videosList );
            }
            
            // open video player on click
            $('ul#videos-list li:visible').click(function(){
                var aurl = $(this).find('span.aurl').text();
                
                TINY.box.show({
                    html : '<div id="flash-player"></div> ',
                    width : 605,
                    height : 405,
                    openjs : function() {
                        // on opening pop-up create instance of video player
                        $f("flash-player", "flowplayer/flowplayer-3.2.16.swf",{
                            clip : {
                                url : aurl,
                                autoPlay : true,
                                autoBuffering: true
                            }
                        });
                    },
                    closejs : function() {
                        // on pop-up window closing destroy video-player
                        $f().unload();
                    }
                }); // endof TINY.box.show
            });
        }
          
    })(token);
}

/**
 * Displays error message
 * 
 * @param {string} msg - Message to show
 */
function showErrorMessage( msg ) {
    TINY.box.show({
        html: msg,
        animate:false,
        close:false,
        mask:false,
        boxid: 'error',
        autohide:4,
        top:-14,
        left:-17
    });
}

