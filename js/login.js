/**
 * Public visible function that handles user login logic
 */
function login() {
    (function(){
        var $loginWindow = $('.login-container');
        var $loginField = $loginWindow.find('input[name="login"]');
        var $passField = $loginWindow.find('input[name="password"]');
        
        // Try to read token from cookies
        var token = readCookie('token');
        if( token !== null && token.length > 0 ) {
            refreshVideosList( token );
        }
        else { // if there's no token - force user to log in
            showLoginWindow();
        }
        
        /**
         * Show login window, collect data from user, send to server, process server responce
         */
        function showLoginWindow() {
                // collect login data
                $loginWindow.show();
                
                // When user tries to send login data
                $loginWindow.find('#login-button').click(function(){
                    // Collect and validate input info
                    var login = $loginField.val();
                    var pass = $passField.val();
                    
                    if( ! validateUserInfo( login, pass ) ) {
                        showErrorMessage( 'Неправильная пара логин-пароль. Будьте внимательнее при наборе');
                        $loginField.val('');
                        $passField.val('');
                        
                        return false;
                    }
                    
                    // If login/pass is correct - try to get token from server
                    sendLoginRequest( login, pass );
                         
                    return false;
                }); //endof click
        }
        
        /**
         * Try to log in user. If succeed - refresh videos list
         * 
         * @param {String} login
         * @param {String} pass
         */
        function sendLoginRequest( login, pass ) {
            $.ajax({
                url: "http://vacancy.dev.telehouse-ua.net/auth/login/",
                crossDomain: true,
                context: document.body,
                type: 'POST',
                data: {
                    login: login,
                    password: pass
                },
                success : function( response ) {
                    //if token is recieved, save it for next 20 minutes
                    createCookie('token', response.token, 20);

                    refreshVideosList(response.token);
                    $loginWindow.hide();
                },
                error : function( response ) {
                    showErrorMessage( 'Пользователь с введенными логином-паролем не найден. Попробуйте еще раз.');
                    $loginField.val('');
                    $passField.val('');
                }
            }); // endof auth/login ajax
        }
        
        /**
         * Some basic validation for login/pass, just as proof of concept.
         * 
         * @param {String} login
         * @param {String} pass
         * @returns {Boolean} isValid
         */
        function validateUserInfo( login, pass ) {
            // some basic validation examples
            var isLoginCorrect = login.length > 2;
            var isPassCorrect = pass.length > 2;
            
            return isLoginCorrect && isPassCorrect;
        }
    })();

}



